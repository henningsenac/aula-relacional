CREATE TABLE IF NOT EXISTS empresas (
    id_empresa INT UNSIGNED NOT NULL AUTO_INCREMENT,
    nome VARCHAR(255) NOT NULL,
    cnpj INT UNSIGNED,
    PRIMARY KEY (id_empresa),
    UNIQUE KEY (cnpj)
);

CREATE TABLE IF NOT EXISTS empresas_unidades (
    id_unidade INT UNSIGNED NOT NULL AUTO_INCREMENT,
    empresa_id INT UNSIGNED NOT NULL,
    cidade_id INT UNSIGNED NOT NULL,
    sede TINYINT(1) NOT NULL,
    PRIMARY KEY (id_unidade),
    FOREIGN KEY (empresa_id) REFERENCES empresas (id_empresa),
    FOREIGN KEY (cidade_id)  REFERENCES cidades (id_cidade)
);

INSERT INTO empresas (
    nome, cnpj
) VALUES(
    'Bradesco', 51752679000106
),(
    'Vale', 63685293000108
),(
    'Cielo', 93972078000159
);

ALTER TABLE empresas MODIFY cnpj VARCHAR(14);

-- Relação N pra N

INSERT INTO empresas_unidades (
    empresa_id, cidade_id, sede
)VALUES
(
    (SELECT id_empresa FROM empresas WHERE nome = 'vale'),
    (SELECT id_cidade FROM cidades WHERE nome = 'caruaru'),
    1
),
(
    (SELECT id_empresa FROM empresas WHERE nome = 'cielo'),
    (SELECT id_cidade FROM cidades WHERE nome = 'niterói'),
    0
),
(
    (SELECT id_empresa FROM empresas WHERE nome = 'bradesco'),
    (SELECT id_cidade FROM cidades WHERE nome = 'campinas'),
    1
),
(
    (SELECT id_empresa FROM empresas WHERE nome = 'bradesco'),
    (SELECT id_cidade FROM cidades WHERE nome = 'juazeiro do norte'),
    0
);



    -- vale caruaru sede
    -- cielo niteroi sem ser sede
    -- bradesco em campinas sede
    -- bradeco em juazeiro sem ser sede
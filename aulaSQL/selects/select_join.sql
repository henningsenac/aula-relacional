SELECT 
    citys.nome AS Cidade,
    states.nome AS Estado,
    regiao AS Região,
    sigla AS Sigla
FROM estados states
left JOIN cidades citys
ON id_estado = estado_id;

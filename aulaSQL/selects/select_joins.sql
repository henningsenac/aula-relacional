-- SELECT * FROM cidades c INNER JOIN prefeitos p ON c.id = p.cidade_id;

-- SELECT * FROM prefeitos p INNER JOIN cidades c ON p.cidade_id = c.id ;



-- INNER JOIN 
SELECT 
    id_cidade AS IdCidade,  
    c.nome AS Cidade, 
    estado_id AS IdEstado, 
    area AS Area,
    id_prefeito AS IdPrefeito,
    p.nome AS Prefeito
FROM cidades c 
INNER JOIN prefeitos p 
ON id_cidade = cidade_id ;

-- LEFT JOIN
SELECT 
    id_cidade AS IdCidade,  
    c.nome AS Cidade, 
    estado_id AS IdEstado, 
    area AS Area,
    id_prefeito AS IdPrefeito,
    p.nome AS Prefeito
FROM cidades c 
LEFT JOIN prefeitos p 
ON id_cidade = cidade_id;

-- RIGHT JOIN
SELECT 
    id_cidade AS IdCidade,  
    c.nome AS Cidade, 
    estado_id AS IdEstado, 
    area AS Area,
    id_prefeito AS IdPrefeito,
    p.nome AS Prefeito
FROM cidades c 
RIGHT JOIN prefeitos p 
ON id_cidade = cidade_id;

-- FULL JOIN 'UNION'
SELECT 
    id_cidade AS IdCidade,  
    c.nome AS Cidade, 
    estado_id AS IdEstado, 
    area AS Area,
    id_prefeito AS IdPrefeito,
    p.nome AS Prefeito
FROM cidades c 
RIGHT JOIN prefeitos p 
ON id_cidade = cidade_id 
UNION
SELECT 
    id_cidade AS IdCidade,  
    c.nome AS Cidade, 
    estado_id AS IdEstado, 
    area AS Area,
    id_prefeito AS IdPrefeito,
    p.nome AS Prefeito
FROM cidades c 
LEFT JOIN prefeitos p 
ON id_cidade = cidade_id;
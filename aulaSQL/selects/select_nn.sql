SELECT e.nome Empresa, c.nome Cidade, c.area Area
FROM empresas e, empresas_unidades eu, cidades c
WHERE id_empresa = empresa_id
AND id_cidade = cidade_id
AND sede = 1
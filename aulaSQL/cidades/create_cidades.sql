CREATE TABLE IF NOT EXISTS cidades (
    id_cidade INT UNSIGNED NOT NULL AUTO_INCREMENT,
    nome VARCHAR(255) NOT NULL,
    estado_id INT UNSIGNED NOT NULL,
    area DECIMAL (10,2),
    PRIMARY KEY (id_cidade),
    FOREIGN KEY (estado_id) REFERENCES estados (id_estado)
);
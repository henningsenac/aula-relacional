INSERT INTO cidades (
    nome, area, estado_id
) VALUES (
    'Campinas', 795, (
        SELECT id_estado FROM estados WHERE nome = 'São Paulo'
        )
)

INSERT INTO cidades (
    nome, area, estado_id
) VALUES(
    'Niterói', 133.9, 19
),(
    'Caruaru', 244.5, 17
),(
    'Juazeiro do Norte', 248.9, (
        SELECT id_estado FROM estados WHERE sigla = 'CE'
        )
)